# Ansible for Dev environment

This is a basic example of ansible for provisioning your dev environment.

## Requirements

Ansible >= 2.3

## How to use

```
$ git clone
$ cd ansible-dev-environment
$ ansible-playbook playbook.yml -i hosts -K
```

That's it.
